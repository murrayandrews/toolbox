# Parent make just runs make in each subdir that has a makefile

# Get a list of all subdirs with a makefile - remove trailing /
SUBDIRS = $(subst /,,$(dir $(wildcard */[mM]akefile)))

# Extract top level make targets
TARGETS = $(shell make -f .makefile __global_targets__)

.PHONY: all $(TARGETS)

help:
	@echo Available make targets are: $@ $(TARGETS)

$(TARGETS):
	@for dir in $(SUBDIRS); \
	do \
		$(MAKE) -C $$dir $@ || break; \
	done
