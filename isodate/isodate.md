# isodate(1) -- print current date & time in ISO 8601 format

## SYNOPSIS

`isodate` \[options\]

## DESCRIPTION

Print the current date and time in ISO 8601 format.

## OPTIONS

*   `-h`, `--help`:
    Print help and exit.

*   `-u`:
    Show UTC rather than local time.

## EXAMPLES

*   `UTC`:
    2015-11-21T21:34:24.280Z

*   `Local`:
    2015-11-21T08:34:24.280+1100


## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
