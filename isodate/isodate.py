#!/usr/bin/env python
"""
Print the current date and time in ISO 8601 format

All rights reserved (Murray Andrews 2015)

"""

from __future__ import print_function
import argparse
import sys
import time
from datetime import datetime
from os.path import basename

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])


# ------------------------------------------------------------------------------
def iso_date(utc=False):
    """
    Return the current date and time in ISO 8601 format.

    :param utc:     If True, use UTC time otherwise local time. Default False.
    :type utc:      bool

    :return:        A atring in UTC 8601 format.
    :rtype:         str
    """

    utc_now = datetime.utcnow()
    if utc:
        return utc_now.isoformat()[:-3] + 'Z'

    # Local time - need to work out UTC offset.
    local_now = datetime.now()
    utc_offset = -(time.altzone if time.daylight else time.timezone)
    tz_str = '{}{:02d}{:02d}'.format(
        '-' if utc_offset < 0 else '+',
        abs(utc_offset) // 3600,
        (abs(utc_offset) // 60) % 60
    )
    return local_now.isoformat()[:-3] + tz_str


# ------------------------------------------------------------------------------
def main():
    """
    Do everything.

    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Print current date/time in ISO 8601 format'
    )
    argp.add_argument('-u', '--utc', action='store_true',
                      help='Show UTC rather than local time')

    args = argp.parse_args()
    print(iso_date(utc=args.utc))


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
