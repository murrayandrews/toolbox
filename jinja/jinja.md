# jinja(1) -- render a Jinja2 template

## SYNOPSIS

`jinja` \[options\] [<template> ... ]

## DESCRIPTION

**Jinja** renders a Jinja2 template with specified parameters. This is
effectively a command line interface to the Jinja2 templating engine.

**Jinja** is useful for automating the generation of repetitive text elements
based on variable data substitution.

## OPTIONS

*   <template>:
    Name of a file containing a Jinja2 template. If not specified or -, then
    read the template from stdin. If multiple templates are specified, the
    rendered output of each is concatenated. See also [ENVIRONMENT VARIABLES].

*   `-h`, `--help`:
    Print help and exit.
    
*   `-d` <delimiter>, `--delimiter` <char>:
    Set the Jinja2 delimiters instead of the default {{...}}, {%...%}, {#...#}.
    Only the first character is used and it will replace the outer curly bracket
    only. If the character selected has a natural left and right variant then
    the obvious pairing is used. Otherwise the same character is used for
    opening and closing. This option is useful if the text to be rendered is
    itself Jinja which must be left untouched.

*   `-f` <param-file>, `--file` <param-file>:
    Get parameters from the specified file. File names with a _.json_ or _.jsn_
    suffix are assumed to be in JSON format, otherwise YAML format is assumed.
    Any parameters specified by `-p`/`--param` options or `-l`/`--list` options
    will override values in the param file. Can be specified multiple times.
    See also [ENVIRONMENT VARIABLES].

*   `-l` <name>=<file>, `--list` <name>=<file>:
    Get the values of a list parameter from the specified file, one value per
    line. If - then read stdin.  Multiple list parameters can be specified using
    multiple `-l`/`--list` arguments. See also [ENVIRONMENT VARIABLES].

*   `-p` <name>=<value>, `--param` <name>=<value>:
    Set the value of a parameter to be fed in to the Jinja template. Multiple
    parameters can be specified using multiple `-p`/`--param` arguments.

*   `-P`, `--no-path`:
    Do not search for list, param and template files in <JINJAPATH>.
    See also [ENVIRONMENT VARIABLES].

A number of options allow stdin in to be specified as -. This can only be used
once.

## BUILTIN PARAMETERS

**Jinja** provides a number of builtin parameters. These are placed in a
dictionary named <jinja>. Creation of this dictionary can be suppressed
using the `-B`/`--no-builtins` option.

*   ctime:
    Current local date/time in ctime(3) format.

*   datetime:
    Current local date/time in YYYY-mm-dd HH:MM:SS format.

*   iso_datetime:
    Current date/time in ISO8601 format.

*   prog:
    Name of the program.

*   user:
    Current user name.

*   utc_ctime:
    Current UTC date/time in ctime(3) format.

*   utc_datetime:
    Current UTC date/time in YYYY-mm-dd HH:MM:SS format.

**Jinja** also makes the environment variables availab;e in a dictionary named
<env>. Creation of this dictionary can be suppressed using the
`-E`/`--no-environ` option.

## ENVIRONMENT VARIABLES

**Jinja** supports the following environment variables.

*   JINJAPATH:
    Specifies a search path for list, parameter and template files.
    The default is _:/usr/local/lib/jinja_ (i.e. current directory
    followed by _/usr/local/lib/jinja_). Path based file searching
    can be suppressed using the `-P`/`--no-path` option.


## SEE ALSO

http://jinja.pocoo.org

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
