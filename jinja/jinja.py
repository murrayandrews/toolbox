#!/usr/bin/env python3
"""
Command line inteface for Jinja2.

Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import jinja2
import yaml
import argparse
import sys
import os
from os.path import basename, splitext
import json
from datetime import datetime
import getpass

__author__ = 'Murray Andrews'

PROG = splitext(basename(sys.argv[0]))[0]

# Default path is current dir followed by /usr/local/lib/PROG
DEFAULT_PATH = ':' + os.path.join('/usr/local/lib', PROG)
PATH_ENV_VAR = PROG.upper() + 'PATH'

# Match delimiter options
DELIMS = {
    '{': '}',
    '<': '>',
    '[': ']',
    '(': ')'
}


# ..............................................................................
# region built in params
# ..............................................................................


# ------------------------------------------------------------------------------
def _param_datetime():
    """
    Return a dictionary of date/time related params.
    :return:        Date/time related params
    :rtype:         dict[str, str]
    """

    return {
        'ctime': str(datetime.now().ctime()),
        'datetime': str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
        'utc_ctime': str(datetime.utcnow().ctime()),
        'utc_datetime': str(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')),
        'iso_datetime': str(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'),
    }


# ------------------------------------------------------------------------------
def _param_psub():
    """
    Return parameters about psub itself.

    :return:        Program name
    :rtype:         dict[str, str]
    """

    return {'prog': PROG}


# ------------------------------------------------------------------------------
def _param_user():
    """
    Return parameters about the current user.

    :return:        User parameters
    :rtype:         dict[str, str]

    """

    return {'user': getpass.getuser()}


# ------------------------------------------------------------------------------
def builtin_params():
    """
    Return a dictionary of all the internally defined params. This is done by
    discovering all functions that have a name of the form _param_xxx. Each of
    which must yield its own dictionary of params.

    :return:    A dictionary of builton parameters.
    :rtype:     dict[str, str]
    """

    gbls = globals()

    params = {}

    for g in gbls:
        if g.startswith('_param_') and hasattr(gbls[g], '__call__'):
            params.update((gbls[g])())

    return params


# ..............................................................................
# endregion built in params
# ..............................................................................


# ------------------------------------------------------------------------------
class StoreNameValuePair(argparse.Action):
    """
    Used with argparse to store values from options of the form:
        --option name=value

    The destination (self.dest) will be created as a dict {name: value}. This
    allows multiple name-value pairs to be set for the same option.

    Usage is:

        argparser.add_argument('-x', metavar='key=value', action=StoreNameValuePair)

    or
        argparser.add_argument('-x', metavar='key=value ...', action=StoreNameValuePair,
                               nargs='+')

    """

    # --------------------------------------------------------------------------
    def __call__(self, parser, namespace, values, option_string=None):

        if not hasattr(namespace, self.dest) or not getattr(namespace, self.dest):
            setattr(namespace, self.dest, dict())
        argdict = getattr(namespace, self.dest)

        if not isinstance(values, list):
            values = [values]
        for val in values:
            try:
                n, v = val.split('=', 1)
            except ValueError as e:
                raise argparse.ArgumentError(self, str(e))
            argdict[n] = v


# ------------------------------------------------------------------------------
def render_file(filename, **kwargs):
    """
    Render the contents of the given Jinja2 template file.

    :param filename:        Name of the file
    :param kwargs:          Parameters supplied to the renderer.
    :return:
    """

    with open(filename, 'r') as jfp:
        jt = jinja2.Template(jfp.read())
        return jt.render(**kwargs)


# ------------------------------------------------------------------------------
def find_in_path(filename, path):
    """
    Find a file in a path. If an absolute path is provided in filename then it
    is merely checked for existance and the absolute path will be returned if it
    exists. Otherwise the path will be searched for the file and if it exists in
    the path, the absolute path will be returned.

    :param filename:    The name of the file to find.
    :param path:        The path. May be either a string of : separated
                        directories or an iterable of dir names. An empty path
                        ('', [] or None) is treated as current directory only.
    :type filename:     str
    :return:            The absolute path of the file if found otherwise None.
    :rtype:             str

    """

    if not filename:
        raise ValueError('filename must be specified')

    if os.path.isabs(filename):
        return filename if os.path.exists(filename) else None

    if not path:
        path = ['.']
    elif isinstance(path, str):
        path = path.split(':')

    for d in path:
        p = os.path.join(d, filename)
        if os.path.exists(p):
            return os.path.abspath(p)

    return None


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Render a Jinja template with specified parameters.'
    )

    argp.add_argument('-B', '--no-builtins', action='store_false', dest='builtins',
                      default=True,
                      help='Do not supply any built-in parameters.')

    argp.add_argument('-E', '--no-environ', action='store_false', dest='environ',
                      default=True,
                      help='Do not supply include the environment variables in'
                           ' the parameters.')

    argp.add_argument('-d', '--delimiter', metavar='CHAR', action='store', default='{',
                      help='Set the Jinja2 delimiters instead of the default {{...}},'
                           ' {%%...%%}, {#...#}. Only the first character is used and'
                           ' it will replace the outer curly bracket only. If the'
                           ' character selected has a natural left and right variant'
                           ' then the obvious pairing is used. Otherwise the same'
                           ' character is used for opening and closing. This option'
                           ' is useful if the text to be rendered is itself Jinja'
                           ' which must be left untouched.')

    argp.add_argument('-l', '--list', action=StoreNameValuePair, metavar='name=file',
                      help='Get the values of a list parameter from the specified'
                           ' file, one value per line. If - then read stdin.'
                           ' Multiple list parameters can be specified using'
                           ' multiple -l/--list arguments. Only one file can be'
                           ' stdin and then only if the template is not read from'
                           ' stdin.')

    argp.add_argument('-f', '--file', action='append', metavar='param-file',
                      help='Get parameters from the specified file. File names'
                           ' with a .json or .jsn suffix are assumed to be in JSON'
                           ' format, otherwise YAML format is assumed. Any'
                           ' parameters specified by -p/--param options or'
                           ' -l/--list options will override values in the'
                           ' param file. Can be specified multiple times.')

    argp.add_argument('-p', '--param', action=StoreNameValuePair, metavar='name=value',
                      help='Set the value of a parameter to be fed in to the Jinja'
                           ' template. Multiple parameters can be specified using'
                           ' multiple -p/--param arguments.')

    argp.add_argument('-P', '--no-path', action='store_false', dest='use_path',
                      default=True,
                      help='Suppress search for list and parameter files in {}.'.format(
                          PATH_ENV_VAR))

    argp.add_argument('template', action='store', nargs='*',
                      help='Name of the file containing the Jinja template. If not'
                           ' specified or -, read the template from stdin.')

    args = argp.parse_args()

    if args.list:
        stdin_used = args.template is sys.stdin
        for f in args.list.values():
            if f == '-':
                if stdin_used:
                    raise Exception('Only one input can be from stdin')
                stdin_used = True

    return args


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    :return:        Status
    :rtype:         int
    """

    args = process_cli_args()

    render_params = {}

    if args.builtins:
        render_params['jinja'] = builtin_params()

    if args.environ:
        render_params['env'] = os.environ

    path = os.environ.get(PATH_ENV_VAR, DEFAULT_PATH) if args.use_path else None

    # ----------------------------------------
    # Process parameter files -- JSON or YAML. Use the search path.
    if args.file:
        for paramfile in args.file:
            f = find_in_path(paramfile, path) if path else paramfile
            if not f:
                raise Exception('No such file or directory: {}'.format(paramfile))

            loader = json.load if splitext(f)[1].lower() in ('.json', '.jsn') else yaml.safe_load

            with open(f, 'r') as fp:
                render_params.update(loader(fp))

    # ----------------------------------------
    # Process command line specified params
    if args.param:
        render_params.update(args.param)

    # ----------------------------------------
    # Process list files. Use the search path.
    if args.list:
        for k, v in args.list.items():
            if v == '-':
                render_params[k] = [l.strip() for l in sys.stdin.readlines()]
            else:
                f = find_in_path(v, path) if path else v
                if not f:
                    raise Exception('No such file or directory: {}'.format(v))
                with open(f, 'r') as fp:
                    render_params[k] = [l.strip() for l in fp.readlines()]

    # ----------------------------------------
    # Setup a Jinja2 environment
    j_start = args.delimiter[0]
    j_end = DELIMS.get(j_start, j_start)
    j_env = jinja2.Environment(
        block_start_string=j_start + '%',
        block_end_string='%' + j_end,
        variable_start_string=j_start + '{',
        variable_end_string='}' + j_end,
        comment_start_string=j_start + '#',
        comment_end_string='#' + j_end
    )

    # ----------------------------------------
    # Read the templates and render them.
    if not args.template:
        # Stdin
        args.template = ['-']

    for template in args.template:
        if template == '-':
            jt = j_env.from_string(sys.stdin.read())
        else:
            f = find_in_path(template, path) if path else template
            if not f:
                raise Exception('No such file or directory: {}'.format(template))
            with open(f, 'r') as fp:
                jt = j_env.from_string(fp.read())
        print(jt.render(**render_params))


# ------------------------------------------------------------------------------
if __name__ == '__main__':

    # exit(main())  # Uncomment for debugging

    try:
        main()
    except Exception as ex:
        print(ex, file=sys.stderr)
        exit(1)

    exit(0)
