#!/bin/sh

set -x
../jinja.py -f param.yaml -f ansi-colours.yaml -l mylist=list1 -p p2=yaml-override template.jinja

../jinja.py -f param.json -f ansi-colours.yaml -l mylist=list1 -p p2=json-override template.jinja
