# j2y(1) -- convert JSON to YAML

## SYNOPSIS

`j2y` [options] [<infile>] [<outfile>]

## DESCRIPTION

**J2y** converts JSON to YAML.

## OPTIONS

*   `-h`, `--help`:
    Print help and exit.

*   `-f` <flow-style>, `--flow-style` <flow-style>:
    Format the resulting YAML using the specified flow style. Allowed
    values are <f> (flow), <b> (block), <h> (hybrid). The default is <h>.

*   `-i` <indent>, `--indent` <indent>:
    Indent blocks by the specified number of spaces. Default 2.

*   `-w` <width>, `--width` <width>:
    Preferred maximum line width. Default 80.

*   <infile>:
    Source JSON file. Default is stdin.

*   <outfile>:
    Output YAML file. Default is stdout.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
