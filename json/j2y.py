#!/usr/bin/env python
"""
Convert JSON on stdin to YAML on stdout.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function
import sys
from os.path import basename
import yaml
import json
import argparse

__author__ = 'Murray Andrews'

FLOW_STYLES = {
    'b': False,  # Enforce block style
    'f': True,  # Enforce flow style. You rarely want this.
    'h': None  # Hybrid.
}
WIDTH = 80
INDENT = 2
PROG = basename(sys.argv[0])

# ------------------------------------------------------------------------------
if __name__ == '__main__':

    argp = argparse.ArgumentParser(prog=PROG, description='Convert JSON to YAML')

    argp.add_argument('-f', '--flow-style', dest='flow_style', action='store',
                      choices=FLOW_STYLES.keys(), default='h',
                      help='Flow style can be b (block), f (flow) or h (hybrid). '
                           'Default is hybrid.')
    argp.add_argument('-i', '--indent', action='store', type=int, default=INDENT,
                      help='Number of spaces to indent. Default {}.'.format(INDENT))
    argp.add_argument('-w', '--width', action='store', type=int, default=WIDTH,
                      help='Target line width. Default {}.'.format(WIDTH))
    argp.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin,
                      help='JSON input file. Default is stdin.')
    argp.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout,
                      help='YAML output file. Default is stdout.')

    args = argp.parse_args(sys.argv[1:])

    obj = None
    try:
        obj = json.load(args.infile)
    except Exception as e:
        print('{}: {}: {}'.format(PROG, args.infile.name, e), file=sys.stderr)
        exit(2)

    try:
        yaml.safe_dump(obj, args.outfile,
                       default_flow_style=FLOW_STYLES[args.flow_style],
                       width=args.width,
                       indent=args.indent)
    except Exception as e:
        print('{}: {}: {}'.format(PROG, args.infile.name, e), file=sys.stderr)
        exit(3)

    exit(0)
