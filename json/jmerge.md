# jmerge(1) -- merge JSON files

## SYNOPSIS

`jmerge` [options] [<infile>...]

## DESCRIPTION

Merge multiple JSON files into one. Input files are processed in order and the
values of the top level keys are progressively merged into an initially empty
object.

Top level keys with object (dictionary) values are merged. Top level keys with
array (list) values are extended. Other (compatible) value types are replaced
unless the `-k` option is specified.

It is an error if the same top level key has different types in any of the 
files.

## OPTIONS

*   `-h`, `--help`:
    Print help and exit.
    
*   `-k`, `--keep-first`:
    By default, values in earlier files are replaced by the same key later
    in the file sequence. This option forces the first seen value to be retained.
    This applies to scalars and values within objects. It does not affect the
    handling of arrays which are always extended.
    
*   `-o` <outfile>, `--out` <outfile>:
    Output file. Default is _stdout_.

*   <infile...>:
    Source JSON file(s). A value of - refers to _stdin_. If no files are specified, _stdin_
    is used.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
