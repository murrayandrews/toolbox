#!/usr/bin/env python

"""
Merge multiple JSON files. The values of the first level keys are merged.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function
import argparse
import json
import sys
from os.path import basename

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])

# ------------------------------------------------------------------------------
if __name__ == '__main__':

    argp = argparse.ArgumentParser(prog=PROG, description='Merge JSON files')

    argp.add_argument('infile', nargs='+', type=argparse.FileType('r'),
                      default=sys.stdin, help='JSON input file. Default is stdin.')
    argp.add_argument('-k', '--keep-first', dest='keep_first', action='store_true',
                      help='Keep first occurrence of scalars.')
    argp.add_argument('-o', '--out', metavar='output', type=argparse.FileType('w'),
                      default=sys.stdout, help='JSON output file. Default is stdout.')

    args = argp.parse_args(sys.argv[1:])

    merged_obj = {}

    # Load the JSON from each file
    for jfile in args.infile:
        obj = None
        try:
            obj = json.load(jfile)
        except Exception as e:
            print('{}: {}: {}'.format(PROG, jfile.name, e), file=sys.stderr)
            exit(2)

        assert isinstance(obj, dict)

        # Merge / replace key values
        for k in obj:

            if k not in merged_obj:
                merged_obj[k] = obj[k]
                continue

            # Existing key - check type compatibility
            if not isinstance(obj[k], type(merged_obj[k])):
                print('{}: {}: incompatible type for key {}'.format(PROG, jfile.name, k),
                      file=sys.stderr)
                exit(3)

            if isinstance(obj[k], dict):
                # Merge dicts
                if args.keep_first:
                    # Current merged object has priority
                    obj[k].update(merged_obj[k])
                    merged_obj[k] = obj[k]
                else:
                    merged_obj[k].update(obj[k])
            elif isinstance(obj[k], list):
                # Extend arrays
                merged_obj[k].extend(obj[k])
            elif not args.keep_first:
                # Replace scalars
                merged_obj[k] = obj[k]

    # Dump the merge result.
    try:
        json.dump(merged_obj, args.out, indent=4, sort_keys=True)
        print('',file=args.out)
    except Exception as e:
        print('{}: Cannot convert to JSON: {}'.format(PROG, e), file=sys.stderr)
        exit(3)
