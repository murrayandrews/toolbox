# y2j(1) -- convert YAML to JSON

## SYNOPSIS

```y2j```

## DESCRIPTION

Convert YAML on stdin to JSON on stdout.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
