# kex(1) -- extract keys from structured data files (JSON, YAML etc.)

## SYNOPSIS

`kex` \[options\] <key> [<file> [<file> ...]]

## DESCRIPTION

**Kex** extracts a specified key from a formatted data file.
Currently supports YAML, JSON and INI (config) files. Kex will try to
determine the file type from the file suffix. If this is not possible, use the
`-t`, `--type` option.

This is useful to allow shell scripts to read a designated
configuration value from a structured file.

## OPTIONS

*   `-h`, `--help`:
    Print help and exit.

*   `-i`, `--ignore`:
    Ignore decoding errors. Default is to abort on errors.

*   `-t` <type>, `--type` <type>:
    Format type (_yaml_, _json_ or _ini_). Any input file whose type cannot be
    determined from the suffix will be assumed to be of this type. This option
    is required when input is supplied via stdin as there is no filename suffix
    to enable determination of format type.

*   <key>:
    The key to extract. The key can specify a recursive descent of the
    configuration data using the form _k1.k2....kn_ (dot separated key hierarchy)
    where _k1_ is the most significant and _kn_ the least.

*   <file> ...:
    One or more data/config files. These will be searched in the specified order
    for the key. The search stops at the first occurrence. A filename of `-`
    refers to stdin. If no file arguments are provided, stdin is used.

## EXAMPLES

Given the YAML file (sample.yaml):

    level1:
        level2a: hello
        level2b: world

The command `kex level1.level2a sample.yaml` will print `hello`.

## AUTHOR

Murray Andrews

## SEE ALSO

JMESPath [](jmespath.org)

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
