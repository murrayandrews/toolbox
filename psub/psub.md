# psub(1) -- substitute parameters in text files

## SYNOPSIS

`psub` [options] [<file> ...]

## DESCRIPTION

**Psub** is a simple command line filter that copies stdin to stdout, replacing
occurrences of parameter references with the value of the parameter.

Parameters can be specified on the command line or in
one or more YAML files specified as a arguments.

## OPTIONS

*   `-a`, `--abort`:
    Abort with an error if any parameters are undefined (i.e. there is no
    parameter value defined). The default behaviour is to leave any undefined
    parameters unsubstituted and continue.

*  `-d` param=<value>, `--define` param=<value>:
    Set the value of the specified parameter to the given <value>. User supplied
    parameter names must start with an alphanumeric and consist of alphanumerics
    and underscores. Parameters specified on the command line will override
    parameters of the same name specified in YAML files.

* `-h`, `--help`:
    Print help and exit.

*  `-l`, `--list`:
    List parameters and exit.
   
*  `-o`, `--off`:
    By default, parameter substitution is on unless turned off by a control
    line (see [CONTROL LINES][] below). This option reverses this behaviour - parameter
    substitution is off until turned on by a control line.
   
*  `-s` <param-style> , `--style` <param-style>:
    Specify the [PARAMETER REFERENCE STYLES][] below.
   
*  file ...:
    One or more YAML files containing parameter values. Unused parameters
    are ignored. If a parameter is specified in more than one YAML file,
    later files take precedence and command line values override file contents.


## PARAMETER REFERENCE STYLES

The `-s` , `--style` option takes a single one-letter parameter that
defines the style for parameter references.

*   `j`:
    Pseudo-Jinja style references: `{{ param }}`.
    Whitespace inside the brackets is ignored.

*   `m`:
    make(1) style references: `$(param)`.
    This is the default for historical reasons.

*   `s`:
    sh(1) style references: `${param}`

## CONTROL LINES

Any line consisting of ```# PSUB OFF``` disables any further substitution.
Any line consisting of ```# PSUB ON``` enables substitution.

If a line contains both _ON_ and _OFF_ commands then all bets are off.
Case and whitespace are ignored in the control commands.

See also the `-o`, `--off` option.

## DEFINING PARAMETERS

In addition to user supplied parameters (via the command line or YAML files),
some parameters are defined by psub itself. These always start with `_`.

Available internally defined parameters can be listed using the
`-l`, `--list` option.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
