#!/usr/bin/env python

"""
This is a simple command line filter that copies stdin to stdout, replacing occurences
of $(xxx) with the value of the parameter named xxx. Parameters can be specified
on the command line with -dxxx=value or in a YAML file specified as an argument.
Parameters for which no substitute is available are left unchanged.

Any line containing # PSUB OFF disables any further substitution.
Any line containing # PSUB ON reenables substitution.
If a line contains both then you're an idiot and all bets are off.
Case and whitespace are ignored in the control commands.

Some parameters are defined by psub itself. These always start with _

Copyright (c) 2014, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import argparse
import getpass
import re
import sys
from datetime import datetime
from os.path import basename

import yaml

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])

# Regexes to match various styles of parameter reference.
# We will replace the {} part in the middle of the regex before use
PARAM_RE = {
    'j': r'{{{{\s*({})\s*}}}}',  # {{ xx }} pseudo Jinja style references
    'm': r'\$\(({})\)',  # $(xx) style param references (like make(1)).
    's': r'\${{({})}}',  # ${xx} style param references (like sh(1)).
}
PARAM_NAME_RE = r'^[a-zA-Z0-9]\w*$'  # User param names must start with alpha-numeric
PSUB_CONTROL_RE = re.compile(r'#\s*PSUB\s*(?P<switch>ON|OFF)', flags=re.IGNORECASE)


# ------------------------------------------------------------------------------
class StoreNameValuePair(argparse.Action):
    """
    Used with argparse to store values from options of the form:
        --option name=value

    The destination (self.dest) will be created as a dict {name: value}. This
    allows multiple name-value pairs to be set for the same option.

    """

    def __call__(self, parser, namespace, values, option_string=None):
        n, v = values.split('=')
        if not re.match(PARAM_NAME_RE, n):
            raise argparse.ArgumentError(self, 'Illegal parameter name: {}'.format(n))
        if not hasattr(namespace, self.dest) or not getattr(namespace, self.dest):
            setattr(namespace, self.dest, dict())
        getattr(namespace, self.dest)[n] = v


# ..............................................................................
# region built in params
# ..............................................................................


# ------------------------------------------------------------------------------
def _param_datetime():
    """
    Return a dictionary of date/time related params.
    :return:        Date/time related params
    :rtype:         dict[str, str]
    """

    return {
        '_ctime': str(datetime.now().ctime()),
        '_datetime': str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
        '_utc_ctime': str(datetime.utcnow().ctime()),
        '_utc_datetime': str(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')),
        '_iso_datetime': str(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'),
    }


# ------------------------------------------------------------------------------
def _param_psub():
    """
    Return parameters about psub itself.

    :return:        Program name
    :rtype:         dict[str, str]
    """

    return {'_prog': PROG}


# ------------------------------------------------------------------------------
def _param_user():
    """
    Return parameters about the current user.

    :return:        User parameters
    :rtype:         dict[str, str]

    """

    return {'_user': getpass.getuser()}


# ..............................................................................
# endregion built in params
# ..............................................................................


# ------------------------------------------------------------------------------
def load_yaml(f):
    """
    Load the specified YAML file and return a dictionary of parameters read from
    the file. All the values must be strings.

    :param f:       Name of the file.
    :type f:        str

    :return:        A dictionary of parameters. May be empty but will be a dict.
    :rtype:         dict[str, str]

    :raise Exception:   If the load fails for some reason or the file is unusable.
    """

    try:
        with open(f, 'r') as fp:
            y = yaml.safe_load(fp)
    except IOError as e:
        raise Exception('{}: {}'.format(f, e.strerror))

    if not y:
        # Ignore files with no stuff in them.
        return {}

    if not isinstance(y, dict):
        raise Exception('{}: Is not a dictionary'.format(f))

    for k in y:
        if not isinstance(k, str):
            raise Exception('{}: Key {} is not a string'.format(f, k))

        if not re.match(PARAM_NAME_RE, k):
            raise Exception('{}: Illegal parameter name: {}'.format(f, k))

        if not isinstance(y[k], str):
            raise Exception('{}: Value for key {} is not a string'.format(f, k))

    return y


# ------------------------------------------------------------------------------
def abort(exit_status, *msg):
    """
    Print a message on stderr and then exit. If exit_status is >= 0 then exit
    with the specified exit_status. If < 0 then return after printing message.

    :param exit_status:     Exit status.
    :param msg:             Msg strings - joined with ' ' and printed to stderr.
    :type exit_status:      int
    :type msg:              str
    """

    print('{}: {}'.format(PROG, ' '.join(msg)), file=sys.stderr)
    if exit_status >= 0:
        exit(exit_status)


# ------------------------------------------------------------------------------
def builtin_params():
    """
    Return a dictionary of all the internally defined params. This is done by
    discovering all functions that have a name of the form _param_xxx. Each of
    which must yield its own dictionary of params.

    :return:    A dictionary of builton parameters.
    :rtype:     dict[str, str]
    """

    gbls = globals()

    params = {}

    for g in gbls:
        if g.startswith('_param_') and hasattr(gbls[g], '__call__'):
            params.update((gbls[g])())

    return params


# ------------------------------------------------------------------------------
def process_stream(params, ifp=sys.stdin, ofp=sys.stdout, style='m',
                   on=True, abort_on_badvar=False):
    """
    Process the specified input stream through to the specified output,
    replacing params with the provided values.

    :param params:  Dictionary of parameters
    :param ifp:     Input file stream. Default stdin.
    :param ofp:     Output file stream. Default stdout.
    :param style:   Specify the style for parameter references. Must be one of
                    the keys in PARAM_RE. Deafult is 'm' make(1) style $(param).
    :param on:      If True, substition is on (enabled) until turned off by a
                    control line. If False, the reverse. Default True.
    :param abort_on_badvar:
                    If True raise an exception if a variable is referenced but
                    not defined
    :type ifp:      file
    :type ofp:      file
    :type style:    str
    :type on:       bool
    :type params:   dict[str, str]
    :type abort_on_badvar: bool
    :raise Exception: If abort_on_badvar is True and an undefined variable is
                    referenced.
    """

    param_re = PARAM_RE[style]
    var_re = re.compile(param_re.format(r'\w+'))  # Generic parameter match

    for line in ifp:
        line = line.rstrip('\n')

        # See if its a control line
        m = PSUB_CONTROL_RE.search(line)
        if m:
            on = m.group('switch').lower() == 'on'
        elif on:
            # See if this line contains any params to substitute

            m = re.findall(var_re, line)

            # Replace any variables - working from the end of the string to the
            # beginning. This should reduce circular / recursive substituion.
            for var in m[::-1]:

                if var in params:
                    # We can replace this one.
                    v_re = param_re.format(var)
                    line = re.sub(v_re, params[var], line, count=1)
                elif abort_on_badvar:
                    raise Exception('Undefined variable: {}'.format(var))

        print(line, file=ofp)


# ------------------------------------------------------------------------------
def main():
    """
    Main processing loop

    """

    # --------------------------------------
    # Parse options
    argp = argparse.ArgumentParser(
        description='Substitute parameters while copying stdin to stdout.',
        prog=PROG)
    argp.add_argument('-a', '--abort', action='store_true',
                      help='Abort if any referenced variables are undefined.'
                           ' Default is to continue.')
    argp.add_argument('-s', '--style', choices=sorted(PARAM_RE.keys()), default='m',
                      help='Parameter references can be in one of the following styles:'
                           ' j - Jinja style {{param}},'
                           ' m - make(1) style $(param),'
                           ' s - sh(1) style ${param}.'
                           ' Default is %(default)s.')
    argp.add_argument('-d', '--define', dest='params',
                      metavar='param=value', action=StoreNameValuePair,
                      help='Set value for specified parameter.')
    argp.add_argument('-l', '--list', action='store_true',
                      help='List parameters and exit.')
    argp.add_argument('-o', '--off', action='store_true',
                      help='Parameter substition is disabled. A "# PSUB ON" control line'
                           ' must be present somewhere in the input to turn it on.')
    argp.add_argument('file', nargs='*', help='YAML file containing params.')

    args = argp.parse_args()

    # ---------------------------------------
    # Setup the internally defined parameters
    params = builtin_params()

    # ---------------------------------------
    # Load the param files
    for f in args.file:
        try:
            params.update(load_yaml(f))
        except Exception as e:
            abort(1, str(e))

    # ---------------------------------------
    # Overlay any command line params
    if args.params:
        params.update(args.params)

    # ---------------------------------------
    if args.list:
        for p in sorted(params):
            print('{}: {}'.format(p, params[p]))
        exit(0)

    # ---------------------------------------
    # Process stdin

    try:
        process_stream(params, style=args.style, on=not args.off,
                       abort_on_badvar=args.abort)
    except KeyboardInterrupt:
        print('\nInterrupt\n', file=sys.stderr)
        exit(3)
    except Exception as e:
        abort(2, str(e))


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
    exit(0)
