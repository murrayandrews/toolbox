# randstr(1) -- generate random strings

## SYNOPSIS

`randstr` \[options\]

## DESCRIPTION

**Randstr** generates a random string. String length and character set
can be selected with command line options.

## OPTIONS

*   `-c` <CHAR_CLASS> ..., `--class` <CHAR_CLASS> ...:
    Specify the character classes from which the random string characters
    will be selected. Each character class is represented by a single
    character (`a`=lower case alpha, `A`=upper case, `0`=digits,
    `+`=punctuation). Multiple class flags can be concatenated in
    a single argument. Default `aA0` (alphanumerics).

*   `-h`, `--help`:
    Print help and exit.

*   `-l` <length>, `--length` <length>:
    Length of the random string. Default 10.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
