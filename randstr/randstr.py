#!/usr/bin/env python

"""
Generate a random string of characters of the specified length.

Copyright (c) 2015, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function
import os
import sys
import string
import argparse
from os.path import basename

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])

char_sources = {
    'a': string.ascii_lowercase,
    'A': string.ascii_uppercase,
    '0': string.digits,
    '+': string.punctuation
}


# ------------------------------------------------------------------------------
class StoreUniqueChars(argparse.Action):
    """
    Used with argparse to store unique characters in the values in a set.

    The destination (self.dest) will be created as a set.

    """

    def __call__(self, parser, namespace, values, option_string=None):
        if not hasattr(namespace, self.dest) or not getattr(namespace, self.dest):
            setattr(namespace, self.dest, set())
        for v in values:
            getattr(namespace, self.dest).update(set(v))


# ------------------------------------------------------------------------------
def random_string(length, *sources):
    """
    Generate a random string of characters. Uses os.urandom so strength is as
    good as urandom. Its claimed this is crypto quality but use at your own peril.

    :param length:          Number of characters.
    :param sources:         One or more strings from which the random characters
                            are selected. Care is taken to remove duplicates from
                            the collection. If none are specified then defaults
                            to the upper and lower case chars and digits. The
                            predefined character sets in the string module are
                            useful here but be careful - do not use string.hexdigits
                            as this contains upper and lower case and will bias
                            the result.

    :type length:           int
    :type sources:
    :return:                A string of "random" characters of the specified length.
    :rtype:                 str
    """

    if sources:
        # Join all the source and keep only unique chars.
        source = ''.join(set(''.join(sources)))
    else:
        source = string.ascii_letters + string.digits

    l = len(source)
    r = []
    for b in os.urandom(length):
        # In Python 2 b is str, in P3 its int already
        # noinspection PyTypeChecker
        n = (b if isinstance(b, int) else ord(b)) % l
        r.append(source[n])

    return ''.join(r)


# ------------------------------------------------------------------------------

if __name__ == '__main__':

    argp = argparse.ArgumentParser(description='Generate a random string', prog=PROG)
    argp.add_argument('-c', '--class', dest='char_class', action=StoreUniqueChars, nargs='+',
                      help='Character classes: a=lower case alpha, A=upper case,'
                           ' 0=digits +=punctuation. (Default aA0)')
    argp.add_argument('-l', '--length', default='10', type=int,
                      help='Length of string (default 10)')

    args = argp.parse_args()

    if not args.char_class:
        args.char_class = 'aA0'

    errs = False
    for c in args.char_class:
        if c not in char_sources:
            errs = True
            print('{}: Invalid character class: {}'.format(PROG, c), file=sys.stderr)

    if not 1 <= args.length <= 1024:
        errs = True
        print('{}: Length must be between 1 and 1024'.format(PROG), file=sys.stderr)
    if errs:
        exit(1)

    srcs = [char_sources[c] for c in args.char_class]
    print(random_string(args.length, *srcs))
