# sqlout(1) -- Process output of tools such as psql into other tabular formats

## SYNOPSIS

`sqlout`\[options\]

## DESCRIPTION

**Sqlout** is a filter that processes the pseudo tabular output of tools such as
psql(1) into other tabular formats. It does a half-hearted, but usually
adequate, job.

## OPTIONS

*   `-f` <format>, `--format` <format>:
    Output format. Allowed values are listed below.

*   `-h`, `--help`:
    Print help and exit.

## OUTPUT FORMATS

The following output formats are supported.

*   `csv`:
    Comma separated values.
    
*   `jira`:
    Jira format markdown table. Output is padded with spaces to attempt
    to maintain alignment.
    
*   `jirac`:
    Compact Jira markdown table. No padding is added.
    
*   `md`:
    Standard markdown (whatever that means these days). Output is padded
    with spaces to attempt to maintain alignment. This is the default.
    
*   `mdc`:
    Compact markdown. No padding is added.
    
*   `psv`:
    Pipe separated values.

## BUGS

If the input data contains pipe (|) in data fields, all bets are off.

## AUTHOR

Murray Andrews

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
