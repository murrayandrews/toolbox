#!/usr/bin/env python

"""
Process output of tools such as psql into other tabular formats.

Input is assumed to look like this

    id     | size0  | number |                 String
-----------+--------+--------+------------------------------------------
 frednurk0 | 142364 |   1147 | Lorem ipsum dolor sit amet, consectetur
 frednurk0 | 141296 |   1165 | Lorem ipsum dolor sit amet, consectetur
 frednurk0 | 129975 |   1962 | Lorem ipsum dolor sit amet, consectetur


--------------------------------------------------------------------------------
Merge multiple JSON files. The values of the first level keys are merged.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import argparse
import csv
import inspect
import sys
from os.path import basename

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])

FORMAT_DEFAULT = 'md'
FORMATS = {
    'md': 'Markdown',
    'csv': 'CSV'
}

formatters = {}


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Process SQL tool output.'
    )

    argp.add_argument(
        '-f', '--format', dest='fmt', action='store',
        choices=sorted(formatters), default=FORMAT_DEFAULT,
        help='Output format. Allowed values are {}. Default is {}.'.format(
            ', '.join(
                ['{} ({})'.format(f, formatters[f].formats[f]) for f in sorted(formatters)]
            ), FORMAT_DEFAULT
        )
    )
    return argp.parse_args()


# ------------------------------------------------------------------------------
def fixed_len_template(s, sep='+'):
    """
    Take a string of the form '-----+----+-----' and work out how far apart the
    + separators are. Return a list of how long each of the dashed sequences are.

    :param s:       The template string.
    :param sep:     The separator. Default '+'
    :type s:        str
    :type sep:      str
    :return:        A list of integers.
    :rtype:         list[int]
    """

    return [len(chunk) for chunk in s.split(sep)]


# ------------------------------------------------------------------------------
def fixed_width_split(s, field_widths, sep_width=0, strip=True):
    """
    Split a field into fields based on each field being a specified width. If
    sep_width is 0, there is no separator between the fields. Otherwise fields
    are assumed to separated by this many characters. The sep_width is not
    included in the field widths.

    :param s:               The string to split.
    :param field_widths:    A list of field widths.
    :param sep_width:       Width of field separator. Default 0.
    :param strip:           If True, strip whitespace from fields. Default True.

    :type s:                str
    :type field_widths:     list[int]
    :type sep_width:        int
    :type strip:            bool

    :return:                list[str]
    """

    fields = []
    field_start = 0
    for fw in field_widths:
        f = s[field_start:field_start + fw]
        fields.append(f.strip() if strip else f)
        field_start += fw + sep_width

    return fields


# ------------------------------------------------------------------------------
def pad_record(field_list, field_widths, fillchar=' '):
    """
    Right pad a list of fields to make sure each field is of the specified width.

    :param field_list:      A list of fields.
    :param field_widths:    A list of field widths. Must be same size as field
                            list.
    :param fillchar:        Fill character. Default is space.

    :type field_list:       list[str]
    :type field_widths:     list[int]
    :type fillchar:         str

    :return:                A list of padded fields.
    :rtype:                 list[str]

    :raise ValueError:      If the field_list and fields_widths are different
                            lengths.
    """

    if len(field_list) != len(field_widths):
        raise ValueError('field_list and field_widths must be same size')

    return [field_list[i].ljust(field_widths[i], fillchar) for i in range(len(field_list))]


# ..............................................................................
# region formatters
# ..............................................................................


# ------------------------------------------------------------------------------
class FormatterBase(object):
    """
    Abstract base class for output formatters. Responsible for writing records
    to the output file.

    """

    formats = {}  # Subclasses must populate this.

    # --------------------------------------------------------------------------
    def __init__(self, fmt, headers, field_widths=None, outfile=None):
        """

        :param fmt:             The output format being processed. Subclasses
                                are allowed to handle multiple formats.
        :param headers:         A list of headers.
        :param field_widths:    A list of field widths. If not specified the
                                widths of the header fields are used.
        :param outfile:         A file object for output. If None use stdout.
                                Default is None.

        :param fmt:             str
        :type headers:          list[str]
        :type field_widths:     list[int]
        :type outfile:          file
        """

        self.format = fmt
        self.headers = headers
        self.field_widths = field_widths if field_widths else [len(h) for h in headers]
        self.outfile = outfile if outfile else sys.stdout

        if len(headers) != len(self.field_widths):
            raise Exception('headers and field_widths have different lengths')

    # --------------------------------------------------------------------------
    def put_record(self, rec):
        """
        Output a record.
        :param rec:             A list of fields.
        :type rec:              list[str]
        """

        raise NotImplementedError('record')

    # --------------------------------------------------------------------------
    def finish(self):
        """
        Do any final cleanup.
        """

        pass


# ------------------------------------------------------------------------------
class FormatMarkdown(FormatterBase):
    """
    Markdown formatter.
    """

    formats = {
        'md': 'Markdown',
        'mdc': 'Compact markdown',
        'jira': 'Jira markdown',
        'jirac': 'Compact Jira markdown'
    }

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """
        Emit the markdown table header.

        """

        super(FormatMarkdown, self).__init__(*args, **kwargs)

        self.compact = self.format.endswith('c')

        hdrs = self.headers if self.compact else pad_record(self.headers, self.field_widths)

        if self.format.startswith('jira'):
            print('||{}||'.format('||'.join(hdrs)))

            # Expand field widths by one for non-compact due to 2 char pipe in header
            if not self.compact:
                self.field_widths = [n + 1 for n in self.field_widths]
        elif self.format.startswith('md'):
            rule = ['-'] * len(self.headers) if self.compact \
                else pad_record([''] * len(self.headers), self.field_widths, fillchar='-')
            print('|{}|'.format('|'.join(hdrs)))
            print('|{}|'.format('|'.join(rule)))
        else:
            raise Exception('Unknown format {} in {}'.format(self.format, self.__class__.__name__))

    # --------------------------------------------------------------------------
    def put_record(self, rec):
        """
        Output a record.
        :param rec:             A list of fields.
        :type rec:              list[str]
        """

        print('|{}|'.format('|'.join(rec if self.compact else pad_record(rec, self.field_widths))))


# ------------------------------------------------------------------------------
class FormatCsv(FormatterBase):
    """
    CSV formatter.
    """

    formats = {
        'csv': 'Comma separated values',
        'psv': 'Pipe separated values'
    }

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """
        Output the CSV header.

        """

        super(FormatCsv, self).__init__(*args, **kwargs)

        if self.format.startswith('csv'):
            delimiter = ','
        elif self.format.startswith('psv'):
            delimiter = '|'
        else:
            raise Exception('Unknown format {} in {}'.format(self.format, self.__class__.__name__))

        self.writer = csv.writer(self.outfile, delimiter=delimiter)
        self.writer.writerow(self.headers)

    # --------------------------------------------------------------------------
    def put_record(self, rec):
        """
        Output a record.

        :param rec:             A list of fields.
        :type rec:              list[str]
        """

        self.writer.writerow(rec)


# ..............................................................................
# endregion formatters
# ..............................................................................


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    """

    # ----------------------------------------
    # Construct the list of formatters by discovering subclasses of FormatterBase.
    # This is a dict keyed on format label with each value a handler class.

    for obj in globals().values():
        if inspect.isclass(obj) and issubclass(obj, FormatterBase) and obj.formats:
            formatters.update({k: obj for k in obj.formats})

    args = process_cli_args()

    # ----------------------------------------
    # Get an output formatter.
    try:
        formatter_class = formatters[args.fmt]
    except KeyError:
        raise NotImplementedError('Format {} is not implemented sorry'.format(args.fmt))

    # ----------------------------------------
    line_no = 0
    header_line = None
    formatter = None
    field_widths = None

    # ----------------------------------------
    # Process lines
    for line in sys.stdin:
        line_no += 1

        if line_no == 1:
            # Header
            header_line = line
            continue

        if line_no == 2:
            # Assumed to be a separator that will give us field lengths.
            if not line.startswith('-'):
                raise Exception('Expected separator on line 2')

            field_widths = fixed_len_template(line)
            formatter = formatter_class(
                fmt=args.fmt,
                headers=fixed_width_split(header_line, field_widths, sep_width=1),
                field_widths=field_widths
            )  # type: FormatterBase
            continue

        # Data line
        formatter.put_record(fixed_width_split(line, field_widths, sep_width=1))

    if formatter:
        formatter.finish()


# ------------------------------------------------------------------------------
if __name__ == '__main__':

    # exit(main())  # Uncomment for debugging

    try:
        main()
    except Exception as ex:
        print(ex, file=sys.stderr)
        exit(1)

    exit(0)
